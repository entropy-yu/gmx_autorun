#!/bin/bash
### © 2022 Entropy-YU ###
### It requires a subscript named "gmx_autorun.ini" with running parameters already defined to run this script.
echo -e "\
 ___________________________________________ \n\
|                                           | \n\
|      GROMACS-Auto-Run  Version 1.0.0      | \n\
|                                           | \n\
| *********** © 2022 Entropy-YU *********** | \n\
|___________________________________________|"

echo -e "\033[?25l"
sleep 0.5
for ((count1=3;count1>=0;count1--))
do
    if ((count1==0));then
        echo '                     GO!'
        sleep 0.5
        break
    fi
    echo -ne '                      '$count1
    sleep 0.5
    echo -ne "\r                       \r"
    sleep 0.5
done
echo -e "\033[?25h"

if [ -f "gmx_autorun.ini" ];then
    source gmx_autorun.ini  # 参数定义自外部脚本gmx_autorun.ini载入
else
    echo '参数定义脚本gmx_autorun.ini缺失!!!'
    exit 1
fi

source $gmx_cuda_path/bin/GMXRC  # 将支持CUDA加速的gmx程序载入环境变量

if [ ! "$structure" ];then
    echo '未定义输入结构文件!!!'
    exit 1
elif [ ! -f "$structure" ];then
    echo '定义的输入结构文件'$structure'不存在!!!'
    exit 1
fi

if [ "$init_cpt" ] && [ ! -f "$init_cpt" ];then
    echo '定义的初始cpt文件'$init_cpt'不存在!!!'
    exit 1
elif [ ! "$init_cpt" ];then
    unset cpt
else
    cpt='-t '$init_cpt
fi

if [ "$index" ] && [ ! -f "$index" ];then
    echo '定义的索引文件'$index'不存在!!!'
    exit 1
elif [ ! "$index" ];then
    unset ndx
else
    ndx='-n '$index  # ndx变量只在em中用到, 因为em不能保证存在有效的索引文件
fi

if [ ! -f "mix.top" ];then
    echo '主拓扑文件mix.top缺失!!!'
    exit 1
fi

if [ "$posre" ] && [ ! -f "$posre"'.gro' ];then
    echo "启用了对"$posre"的限制势, 但"$posre".gro缺失!!!"
    exit 1
fi

if (($em==1));then
    if [ ! -f "em.mdp" ];then
        echo '启用了能量极小化, 但em.mdp缺失!!!'
        exit 1
    fi
    if (($em_cg==1)) && [ ! -f "em_cg.mdp" ];then
        echo '启用了共轭梯度法能量极小化, 但em_cg.mdp缺失!!!'
        exit 1
    fi
    if [ ! $em_nt ];then
        echo '未定义能量极小化时使用的总线程数!!!'
        exit 1
    elif ((em_nt+${pinoffset:=0}>"$(cat /proc/cpuinfo | grep 'processor' | wc -l)"));then
        echo '设置的能量极小化线程数溢出!!!'
        exit 1
    fi
fi

if (($pr==1));then
    if [ ! -f "pr.mdp" ];then
        echo '启用了限制性MD, 但pr.mdp缺失!!!'
        exit 1
    elif [ ! "$(cat pr.mdp | grep DPOSRES | head -1)" ];then
        echo '启用了限制性MD, 但未在pr.mdp中定义“-DPOSRES”!!!'
        exit 1
    elif [ ! "$(cat mix.top | grep posre.itp | head -1)" ];then
        echo '启用了限制性MD, 但未在mix.top中include posre.itp!!!'
        exit 1
    elif [ ! "$posre" ];then
        echo '启用了限制性MD, 但未定义posre值!!!'
        exit 1
    fi
fi

if (($eq_nvt==1)) && [ ! -f "eq_nvt.mdp" ];then
    echo '启用了eq_nvt, 但eq_nvt.mdp缺失!!!'
    exit 1
fi

if (($eq_npt==1)) && [ ! -f "eq_npt.mdp" ];then
    echo '启用了eq_npt, 但eq_npt.mdp缺失!!!'
    exit 1
fi

if (($non_eq==1)) && [ ! -f "non_eq.mdp" ];then
    echo '启用了non_eq, 但non_eq.mdp缺失!!!'
    exit 1
fi

if (($prod==1)) && [ ! -f "prod.mdp" ];then
    echo '启用了prod, 但prod.mdp缺失!!!'
    exit 1
    if (($prod_rerun==1)) && [ ! -f "prod_rerun.mdp" ];then
        echo '启用了prod_rerun, 但prod_rerun.mdp缺失!!!'
        exit 1
    fi
fi

if (($nopbc==1));then
    if [ ! -f "nopbc.mdp" ];then
        echo '启用了nopbc, 但nopbc.mdp缺失!!!'
        exit 1
    fi
    if [ ! -f "nopbc.gro" ];then
        echo '启用了nopbc, 但nopbc.gro缺失!!!'
        exit 1
    fi
    if [ ! -f "nopbc.top" ];then
        echo '启用了nopbc, 但nopbc.top缺失!!!'
        exit 1
    fi
fi

if [ "${pion_name: -1}" == ';' ] || [ "${pion_number: -1}" == ';' ] || [ "${nion_name: -1}" == ';' ] || [ "${nion_number: -1}" == ';' ];then
    echo '参数末尾不能为";"!!!'
    exit 1
fi

if (($pr==1)) || (($eq_nvt==1)) || (($eq_npt==1)) || (($non_eq==1)) || (($prod==1));then
    if (($multi_gpu==1));then
        if [ ! $ntmpi ] || [ ! $ntomp ];then
            echo '启用了MD模拟并选择了多GPU运行, 但未定义ntmpi或ntomp!!!'
            exit 1
        elif ((ntmpi*ntomp+${pinoffset:=0}>"$(cat /proc/cpuinfo | grep 'processor' | wc -l)"));then
            echo 'ntmpi*ntomp溢出!!!'
            exit 1
        else
            nt_set="-ntmpi $ntmpi -ntomp $ntomp -pme gpu -npme 1"  # 定义多GPU模式下的mdrun选项
        fi
        if [ $gpu_id ];then
            if ((${#gpu_id}<2));then
                echo '启用了MD模拟并选择了多GPU运行, 脚本当前采用gpu_id设置, 但gpu_id项未设置多块GPU!!!'
                exit 1
            fi
            if ((${gpu_id: -1}>="$(lspci | grep 'VGA' | wc -l)"));then
                echo '设置的gpu_id溢出!!!'
                exit 1
            fi
            gpu="-gpu_id $gpu_id"
        elif [ $gputasks ];then
            if ((${#gputasks}!=ntmpi));then
                echo '启用了MD模拟并选择了多GPU运行, 脚本当前采用gputasks设置, gputasks设置错误!!!'
                exit 1
            fi
            gpu="-gputasks $gputasks -nb gpu"
        fi
    else
        if [ ! $md_nt ];then
            echo '启用了MD模拟并选择了单GPU运行, 但未定义md_nt!!!'
            exit 1
        elif ((md_nt+${pinoffset:=0}>"$(cat /proc/cpuinfo | grep 'processor' | wc -l)"));then
            echo '设置的md_nt溢出!!!'
            exit 1
        else
            nt_set="-nt $md_nt"  # 定义单GPU模式下的mdrun并行线程参数
        fi
        if ((${#gpu_id}!=1));then
            echo '启用了MD模拟并选择了单GPU运行, 但gpu_id项未设置单块GPU!!!'
            exit 1
        fi
        if ((${gpu_id: -1}>="$(lspci | grep 'VGA' | wc -l)"));then
            echo '设置的gpu_id溢出!!!'
            exit 1
        fi
        if ((update_gpu==1));then
            gpu="-gpu_id $gpu_id -update gpu"
        else
            gpu="-gpu_id $gpu_id"
        fi
    fi
    if [ "${pinstride:=0}" != '0' ];then
        echo 'Warning: 人为指定pinstride可能会影响性能, 甚至导致MD无法运行! 请确认所指定的pinstride值是否合适! 30s内答复, 超时将默认继续执行...'
        while true
        do
            read -r -t 30 -p '[y=继续; n=退出]' request1
            if [ "$request1" == 'y' ] || [ "$request1" == '' ];then
                echo -e "\n3s后开始执行..."
                sleep 3
                break
            elif [ "$request1" == 'n' ];then
                echo '退出...'
                exit 0
            else
                echo '无效输入, 请重试!'
            fi
        done
    fi
fi

if (($sasa==1)) || (($rmsd==1)) || [ "$energy" ] || (($density==1)) || (($msd==1)) || (($rdf==1)) || (($fix_xtc==1)) || (($non_eq==1)) || (($prod_rerun==1));then
    mkdata=1
    if ((ana_parallel_threads>"$(cat /proc/cpuinfo | grep 'processor' | wc -l)"));then
        echo '设置的ana_parallel_threads溢出!!!'
        exit 1
    fi
    # ↓ 使用命名管道结合文件描述符fd3控制数据分析任务并发数
    rm -f ana_fifoFile
    mkfifo ana_fifoFile
    exec 3<> ana_fifoFile
    rm -f ana_fifoFile
    for ((i=0;i<ana_parallel_threads;i++))
    do
        echo '' >&3
    done
fi

if (($non_eq==1));then
    if ((non_eq_ana_threads>"$(cat /proc/cpuinfo | grep 'processor' | wc -l)"));then
        echo '设置的non_eq_ana_threads溢出!!!'
        exit 1
    fi
    if (($non_eq_reeq==1));then
        if [ $interface_type == l_l ];then
            if [ ! -f "eq_npt.mdp" ];then
                echo 'non_eq的interface_type为l_l, 但eq_npt.mdp缺失!!!'
                exit 1
            fi
        elif [ $interface_type == g_l ];then
            if [ ! -f "eq_nvt.mdp" ];then
                echo 'non_eq的interface_type为g_l, 但eq_nvt.mdp缺失!!!'
                exit 1
            fi
        else
            echo 'interface_type不合法!!!'
            exit 1
        fi
    fi
    if ! ((APM_t_dt>=1));then
        let APM_t_dt=100
        echo 'Warning: 设置的APM_t_dt值不合法, 将重置为默认值100'
    fi
    if ! ((eq_time_est_dt>=1));then
        let eq_time_est_dt=25000
        echo 'Warning: 设置的eq_time_est_dt值不合法, 将重置为默认值25000'
    fi
    # ↓ 使用命名管道结合文件描述符fd4控制non_eq数据分析任务并发数
    rm -f non_eq_ana_fifoFile
    mkfifo non_eq_ana_fifoFile
    exec 4<> non_eq_ana_fifoFile
    rm -f non_eq_ana_fifoFile
    for ((i=0;i<non_eq_ana_threads;i++))
    do
        echo '' >&4
    done
fi

if [ "$(top -b -n 1|head -10|grep gmx|head -1)" ];then
    echo '在系统中检测到其他gmx进程, 如何执行本次任务? 30s内答复, 超时将默认为wait...'
    while true
    do
        read -r -t 30 -p '[wait=等待至其他gmx进程结束再执行此任务; now=立即执行; q=退出]' request2
        # ↓ 若答复wait, 则脚本会每隔60s检测1次当前系统有无其他gmx进程在运行, 1次未检测到后隔10s再检测1次, 若同样未检测到, 即等待3s开始运行本任务
        if [ "$request2" == 'wait' ] || [ "$request2" == '' ];then
            echo -e "\n收到wait信号或无回复, 开始等待...\n"
            gmx=1
            while [ "$gmx" ]
            do
                while [ "$gmx" ]
                do
                    sleep 60
                    gmx="$(top -b -n 1|head -10|grep gmx|head -1)"
                done
                sleep 10
                gmx="$(top -b -n 1|head -10|grep gmx|head -1)"
            done
            echo '连续2次未在系统中检测到gmx进程, 3s后开始执行等待中的gmx任务!'
            sleep 3
            break
        elif [ "$request2" == 'now' ];then
            echo '收到now信号, 3s后开始执行...'
            sleep 3
            break
        elif [ "$request2" == 'q' ];then
            echo '收到q信号, 退出...'
            exit 0
        else
            echo '无效输入, 请重试!'
        fi
    done
fi

if [ $wait_proc_id ];then
    echo '等待进程'$wait_proc_id'结束...'
    wait_proc=1
    while [ "$wait_proc" ]
    do
        while [ "$wait_proc" ]
        do
            sleep 60
            wait_proc=`top -b -n 1|grep $wait_proc_id`
        done
        sleep 10
        wait_proc=`top -b -n 1|grep $wait_proc_id`
    done
    echo '连续2次未在系统中检测到'$wait_proc_id'进程, 3s后开始执行等待中的gmx任务!'
    sleep 3
fi

echo 'GROMACS-Auto-Run Started @' $(date +%F%n%T) > gmx_autorun_time.log

# ↓ 输出SASA数据, 再与此前SASA数据合并输出至txt文档, 若当前MD任务为整条MD任务流的首个, 则仅将xvg文档转换为txt文档
function sasa_func() {
    if (($1==1));then
        if [ "$6" ];then
            for tmp in $2
            do
                read -u3
                {
                    echo -e "$tmp"|gmx sasa -f ../$3.xtc -s ../$3.tpr -n ../"$4" -dt $5 -o sasa_grp"$tmp".xvg > sasa_grp"$tmp".out 2>&1
                    cp -p ../"$6"_data/sasa_grp"$tmp".txt .
                    end_frame=`awk 'END {print $1}' sasa_grp"$tmp".txt`
                    awk -v beg_frame=$end_frame 'NR>25 {print $1+beg_frame,$2}' sasa_grp"$tmp".xvg >> sasa_grp"$tmp".txt
                    echo '' >&3
                } &
            done
        else
            for tmp in $2
            do
                read -u3
                {
                    echo -e "$tmp"|gmx sasa -f ../$3.xtc -s ../$3.tpr -n ../"$4" -dt $5 -o sasa_grp"$tmp".xvg > sasa_grp"$tmp".out 2>&1
                    awk 'NR>24 {print $1,$2}' sasa_grp"$tmp".xvg > sasa_grp"$tmp".txt
                    echo '' >&3
                } &
            done
        fi
        wait
    fi
}

# ↓ 以整条MD任务流的首个tpr为参考结构输出RMSD数据, 再与此前RMSD数据合并输出至txt文档, 若当前MD任务为整条MD任务流的首个, 则仅将xvg文档转换为txt文档
function rmsd_func() {
    if (($1==1));then
        if [ "$7" ];then
            for tmp in $2
            do
                read -u3
                {
                    echo -e "$tmp\n$tmp"|gmx rms -f ../$3.xtc -s ../$6.tpr -n ../"$4" -dt $5 -o rmsd_grp"$tmp".xvg > rmsd_grp"$tmp".out 2>&1
                    cp -p ../"$7"_data/rmsd_grp"$tmp".txt .
                    end_frame=`awk 'END {print $1}' rmsd_grp"$tmp".txt`
                    awk -v beg_frame=$end_frame 'NR>19 {print $1+beg_frame,$2}' rmsd_grp"$tmp".xvg >> rmsd_grp"$tmp".txt
                    echo '' >&3
                } &
            done
        else
            for tmp in $2
            do
                read -u3
                {
                    echo -e "$tmp\n$tmp"|gmx rms -f ../$3.xtc -s ../$3.tpr -n ../"$4" -dt $5 -o rmsd_grp"$tmp".xvg > rmsd_grp"$tmp".out 2>&1
                    awk 'NR>18 {print $1,$2}' rmsd_grp"$tmp".xvg > rmsd_grp"$tmp".txt
                    echo '' >&3
                } &
            done
        fi
        wait
    fi
}

function energy_func() {
    if [ "$1" ];then
        for tmp in $1
        do
            if [[ $tmp == *'\n'* ]];then  # 防止当$energy字符串中含有'\n'时输出的文件名称在某些环境下不合法
                tmp2="${tmp%%'\n'*}"_etc
            else
                tmp2="$tmp"
            fi
            read -u3
            {
                echo -e "$tmp"|gmx energy -f ../$2.edr -o energy_"$tmp2".xvg -b $3 > energy_"$tmp2".out 2>&1
                echo '' >&3
            } &
        done
        wait
    fi
}

function density_func() {
    if (($1==1));then
        for tmp in $3
        do
            read -u3
            {
                echo -e "$tmp"|gmx density -f ../$2.xtc -s ../$2.tpr -o density_grp"$tmp".xvg -sl $4 -b $5 -n ../"$6" > density_grp"$tmp".out 2>&1
                echo '' >&3
            } &
        done
        wait
    fi
}

function fix_xtc_func() {
    if (($1==1));then
        read -u3
        {
            echo -e "0"|gmx trjconv -f ../$2.xtc -s ../$2.tpr -o $2_fixed.xtc -pbc mol > fix_xtc.out 2>&1
            echo '' >&3
        } &
        wait
    fi
}

function hbond_func() {
    if (($1==1));then
        mkdir hbond
        cd hbond
        mkfifo ana_fifoFile
        exec 5<> ana_fifoFile
        rm -f ana_fifoFile
        for ((i=0;i<$4;i++))
        do
            echo '' >&5
        done
        for tmp in $2
        do
            read -u5
            {
                echo -e "$3\n$tmp"|gmx hbond -f ../../$5.xtc -s ../../$5.tpr -n ../../"$6" $7 -num hbnum_grp"$3"_"$tmp".xvg > hbnum_grp"$3"_"$tmp".out 2>&1
                echo -n "$3"'    '"$tmp"'    ' >> hbnum.txt
                cat hbnum_grp"$3"_"$tmp".out | grep 'Average number of hbonds per timeframe' | awk 'NR==1 {print $7}' >> hbnum.txt
                echo '' >&5
            } &
        done
        wait
        exec 5>&-
        cd ..
    fi
}

mw='-maxwarn 1'  # "-maxwarn"操作用来防止grompp由于 输入的结构文件中原子名称与拓扑文件中定义的不一致 或 没有补充离子至电中性 引起warning过多从而报错中止。有时不运行完整的任务流，故所有grompp操作中都会用此选项。

if (($solvate==1));then
    gmx solvate -cs ${sol_struc:=spc216.gro} -cp "$structure" -o mixsol.gro -p mix.top
    structure='mixsol.gro'
fi
# ↓ 补充离子
if (($genion==1));then
    if [ "$pion_name" ];then
        let i=$(echo $pion_name | grep -o ';' | wc -l)+1
    fi
    if [ "$nion_name" ];then
        let j=$(echo $nion_name | grep -o ';' | wc -l)+1
    fi
    while ((i>0))
    do
        let i--
        pname=${pion_name%%';'*}
        np=${pion_number%%';'*}
        gmx grompp -f em.mdp -c "$structure" -p mix.top -o genion_"$i"_"$j".tpr -maxwarn 2
        structure=genion_"$i"_"$j".gro
        echo -e "SOL"|gmx genion -s ${structure%.*}.tpr -o $structure -p mix.top -pname $pname -np $np
        pion_name=${pion_name#*';'}
        pion_number=${pion_number#*';'}
        if ((i==0)) && ((j==0));then
            mv $structure init.gro
            structure="init.gro"
        fi
    done
    while ((j>0))
    do
        let j--
        nname=${nion_name%%';'*}
        nn=${nion_number%%';'*}
        gmx grompp -f em.mdp -c "$structure" -p mix.top -o genion_"$i"_"$j".tpr -maxwarn 2
        structure=genion_"$i"_"$j".gro
        echo -e "SOL"|gmx genion -s ${structure%.*}.tpr -o $structure -p mix.top -nname $nname -nn $nn
        pion_name=${nion_name#*';'}
        pion_number=${nion_number#*';'}
        if ((j==0));then
            mv $structure init.gro
            structure="init.gro"
        fi
    done
    rm genion_*
    unset mw  # 在结构正常后就不需要了, 直接设为空值。此后还会有该语句, 因为考虑到有时不跑全流程, 便在非最后一步(prod)的每一次grompp中都加这样一条。
fi

if [ ${structure##*.} != gro ];then
    gmx editconf -f "$structure" -o init.gro
    structure="init.gro"
fi

# ↓ 制作限制势itp
if [ "$posre" ];then
    echo -e "0" |gmx genrestr -f "$posre".gro
fi

# ↓ 能量极小化
if (($em==1));then
    echo 'Energy Minimization Started @' $(date +%F%n%T) >> gmx_autorun_time.log
    if (($em_double_precision==1));then
        source $gmx_double_path/bin/GMXRC
        gmx_d grompp -f em.mdp -c "$structure" $ndx -p mix.top -o em.tpr $mw
        unset mw
        gmx_d mdrun -v -deffnm em -nt $em_nt -pin on -pinoffset $pinoffset -pinstride ${pinstride:=0}
        structure='em.gro'
        if (($em_cg==1));then
            gmx_d grompp -f em_cg.mdp -c "$structure" $ndx -p mix.top -o em_cg.tpr
            gmx_d mdrun -v -deffnm em_cg -nt $em_nt -pin on -pinoffset $pinoffset -pinstride $pinstride
            structure='em_cg.gro'
        fi
        source $gmx_cuda_path/bin/GMXRC
    else
        gmx grompp -f em.mdp -c "$structure" $ndx -p mix.top -o em.tpr $mw
        unset mw
        gmx mdrun -v -deffnm em -nt $em_nt -pin on -pinoffset $pinoffset -pinstride ${pinstride:=0}
        structure='em.gro'
        if (($em_cg==1));then
            gmx grompp -f em_cg.mdp -c "$structure" $ndx -p mix.top -o em_cg.tpr
            gmx mdrun -v -deffnm em_cg -nt $em_nt -pin on -pinoffset $pinoffset -pinstride $pinstride
            structure='em_cg.gro'
        fi
    fi
    echo 'Energy Minimization Finished @' $(date +%F%n%T) >> gmx_autorun_time.log
fi

# ↓ 若未定义索引文件, 则在此处制作默认索引文件
if [ ! "$index" ];then
    echo q|gmx make_ndx -f "$structure"
    index='index.ndx'
fi

# ↓ 限制性MD模拟
if (($pr==1));then
    current_md=pr
    echo 'Restrained MD Started @' $(date +%F%n%T) >> gmx_autorun_time.log
    gmx grompp -f pr.mdp -c "$structure" -r "$structure" $cpt $ndx -p mix.top -o pr.tpr -maxwarn 1
    unset mw
    gmx mdrun -v -deffnm pr -pin on -pinoffset $pinoffset -pinstride $pinstride $gpu $nt_set
    echo 'Restrained MD Finished @' $(date +%F%n%T) >> gmx_autorun_time.log
    structure='pr.gro'
    cpt='-t pr.cpt'
    if (($mkdata==1)) ;then
        mkdir pr_data 
        cd pr_data
        sasa_func "$sasa" "$sasa_grp" "$current_md" "$index" "${sasa_dt:=0}" "$last_mkdata" &
        rmsd_func "$rmsd" "$rmsd_grp" "$current_md" "$index" "${rmsd_dt:=0}" "$first_mkdata" "$last_mkdata" &
        first_mkdata="$current_md"
        last_mkdata="$current_md"
    fi
fi

# ↓ 平衡相MD模拟(NVT)
if (($eq_nvt==1));then
    if ((ana_parallel_wait==1));then
        wait
    fi
    current_md=eq_nvt
    echo 'Pre-equilibrium-NVT MD Started @' $(date +%F%n%T) >> gmx_autorun_time.log
    gmx grompp -f eq_nvt.mdp -c "$structure" $cpt $ndx -p mix.top -o eq_nvt.tpr $mw
    unset mw
    gmx mdrun -v -deffnm eq_nvt -pin on -pinoffset $pinoffset -pinstride $pinstride $gpu $nt_set
    echo 'Pre-equilibrium-NVT MD Finished @' $(date +%F%n%T) >> gmx_autorun_time.log
    structure='eq_nvt.gro'
    cpt='-t eq_nvt.cpt'
    if (($mkdata==1)) ;then
        # 注意: 当mdp文件'nsteps'或'dt'字段前还有'nsteps'或'dt'字符串时, 以及'nsteps'或'dt'字段后有注释时, 均会出错, 后同。
        tmp=`cat eq_nvt.mdp | grep nsteps | head -1` && ns=${tmp#*'='}
        tmp=`cat eq_nvt.mdp | grep dt | head -1` && dt=${tmp#*'='}
        time=`echo | awk "{print $ns*$dt}"`
        if ((time<=length));then
            let tbeg=0
        else
            let tbeg=time-length
        fi
        mkdir eq_nvt_data
        cd eq_nvt_data
        sasa_func "$sasa" "$sasa_grp" "$current_md" "$index" "${sasa_dt:=0}" "$last_mkdata" &
        rmsd_func "$rmsd" "$rmsd_grp" "$current_md" "$index" "${rmsd_dt:=0}" "$first_mkdata" "$last_mkdata" &
        energy_func "$energy" "$current_md" "$tbeg" &
        density_func "$density" "$current_md" "$density_grp" "${density_sl:=200}" "$tbeg" "$index" &
        fix_xtc_func "$fix_xtc" "$current_md" &
        hbond_func "$hbond" "$hbond_grp_a" "$hbond_grp_b" "$hbond_parallel_threads" "$current_md" "$index" "$hbond_exoption"
        if [ ! "$last_mkdata" ];then
            first_mkdata="$current_md"
        fi
        last_mkdata="$current_md"
        cd ..
        echo 'Pre-equilibrium-NVT Data Analysis Finished @' $(date +%F%n%T) >> gmx_autorun_time.log
    fi
fi

# ↓ 平衡相MD模拟(NPT)
if (($eq_npt==1));then
    if ((ana_parallel_wait==1));then
        wait
    fi
    current_md=eq_npt
    echo 'Pre-equilibrium-NPT MD Started @' $(date +%F%n%T) >> gmx_autorun_time.log
    gmx grompp -f eq_npt.mdp -c "$structure" $cpt $ndx -p mix.top -o eq_npt.tpr $mw
    unset mw
    gmx mdrun -v -deffnm eq_npt -pin on -pinoffset $pinoffset -pinstride $pinstride $gpu $nt_set
    echo 'Pre-equilibrium-NPT MD Finished @' $(date +%F%n%T) >> gmx_autorun_time.log
    structure='eq_npt.gro'
    cpt='-t eq_npt.cpt'
    if (($mkdata==1)) ;then
        tmp=`cat eq_npt.mdp | grep nsteps | head -1` && ns=${tmp#*'='}
        tmp=`cat eq_npt.mdp | grep dt | head -1` && dt=${tmp#*'='}
        time=`echo | awk "{print $ns*$dt}"`
        if ((time<=length));then
            let tbeg=0
        else
            let tbeg=time-length
        fi
        mkdir eq_npt_data
        cd eq_npt_data
        sasa_func "$sasa" "$sasa_grp" "$current_md" "$index" "${sasa_dt:=0}" "$last_mkdata" &
        rmsd_func "$rmsd" "$rmsd_grp" "$current_md" "$index" "${rmsd_dt:=0}" "$first_mkdata" "$last_mkdata" &
        energy_func "$energy" "$current_md" "$tbeg" &
        density_func "$density" "$current_md" "$density_grp" "${density_sl:=200}" "$tbeg" "$index" &
        fix_xtc_func "$fix_xtc" "$current_md" &
        hbond_func "$hbond" "$hbond_grp_a" "$hbond_grp_b" "$hbond_parallel_threads" "$current_md" "$index" "$hbond_exoption"
        if [ ! "$last_mkdata" ];then
            first_mkdata="$current_md"
        fi
        last_mkdata="$current_md"
        cd ..
        echo 'Pre-equilibrium-NPT Data Analysis Finished @' $(date +%F%n%T) >> gmx_autorun_time.log
    fi
fi

# ↓ 非平衡-平衡MD模拟(NPgammaT)
if (($non_eq==1));then
    if ((ana_parallel_wait==1));then
        wait
    fi
    current_md=non_eq
    echo 'Non-equilibrium to Equilibrium MD Started @' $(date +%F%n%T) >> gmx_autorun_time.log
    gmx grompp -f non_eq.mdp -c "$structure" $cpt $ndx -p mix.top -o non_eq.tpr $mw
    unset mw
    gmx mdrun -v -deffnm non_eq -pin on -pinoffset $pinoffset -pinstride $pinstride $gpu $nt_set
    echo 'Non-equilibrium to Equilibrium MD Finished @' $(date +%F%n%T) >> gmx_autorun_time.log
    tmp=`cat non_eq.mdp | grep nsteps | head -1` && ns=${tmp#*'='}
    tmp=`cat non_eq.mdp | grep dt | head -1` && dt=${tmp#*'='}
    time=`echo | awk "{print $ns*$dt}"`
    if ((time<=length_non_eq));then
        let tbeg=0
    else
        let tbeg=time-length_non_eq
    fi
    mkdir non_eq_data
    cd non_eq_data
    # ↓ 估计平衡时间t_eq
    crtc=`echo | awk '{printf "%.0f",10000*c}' c=$eq_time_est_critical`
    for ((t=eq_time_est_dt;t<time;t=t+eq_time_est_dt))
    do
        echo 'box-x' | gmx energy -f ../non_eq.edr -b $t -o energy_tmp_box_x.xvg > energy_tmp_box_x.out 2>&1
        drift=`cat energy_tmp_box_x.out | tail -1 | awk 'NR==1 {printf "%.0f",10000*sqrt($5*$5)}'`
        rm energy_tmp_box_x.*
        if ((drift<=crtc));then
            let t_eq=t
            echo 't_eq = '"$t_eq"' ps' > t_eq="$t_eq".txt
            break
        fi
    done
    unset crtc
    # ↓ 输出non_eq过程的盒子XY边长-时间曲线和单分子面积-时间关系曲线
    echo -e 'box-x\nbox-y' | gmx energy -f ../non_eq.edr -o energy_box_xy.xvg > energy_box_xy.out 2>&1
    awk 'NR>=26 {printf $1 "    " "%.10f\n",$2*$3*100/n}' n=$n_surfactant energy_box_xy.xvg > APM-t_full.txt
    awk '{if ($1%dt==0) {print $1 "    " $2}}' dt=$APM_t_dt APM-t_full.txt > APM-t_rough.txt
    APM_min=`awk '{if ($1>=teq) {sum=sum+$2;i++}} END {printf "%.10f",sum/i}' teq=$t_eq APM-t_full.txt`
    echo $APM_min > APM_min="$APM_min".txt
    echo 'Area Per Molecule - Time Graph Generated @' $(date +%F%n%T) >> ../gmx_autorun_time.log
    # ↓ 寻找APM最接近APM_min的一帧, 输出之, 作为prod初始结构
    tmp=`cat ../non_eq.mdp | grep dt | head -1` && dt=${tmp#*'='}
    tmp=`cat ../non_eq.mdp | grep nstxout-compressed | head -1` && nstx=${tmp#*'='}
    non_eq_xtc_dt=`echo | awk "{print $dt*$nstx}"`
    t_optimal=`awk '
    BEGIN {d_min=65535}
    {
        if ($1%xdt==0 && $1>=teq) {
            if ($2-a>=0 && ($2-a)<=d_min) {d_min=$2-a;t=$1}
            if ($2-a<0 && (a-$2)<=d_min) {d_min=a-$2;t=$1}
        }
    }
    END {printf "%.3f",t}
    ' a=$APM_min teq=$t_eq xdt=$non_eq_xtc_dt APM-t_full.txt`
    echo -e 0 | gmx trjconv -f ../non_eq.xtc -s ../non_eq.tpr -pbc mol -b $t_optimal -dump $t_optimal -o ../non_eq_"$t_optimal".gro > trjconv.out 2>&1
    structure=non_eq_"$t_optimal".gro
    unset cpt
    # ↓ 提取输出non_eq过程的界面厚度-时间曲线所需的原始数据
    if [ $non_eq_thick_dt ] && ((non_eq_thick_dt!=0));then
        mkdir thickness-t
        cd thickness-t
        let steps=time/non_eq_thick_dt
        for ((i=0;i<steps;i++))
        do
            {
                read -u4
                suffix="$((i*non_eq_thick_dt+1))"_"$(((i+1)*non_eq_thick_dt))"
                echo -e "2"|gmx density -f ../../non_eq.xtc -s ../../non_eq.tpr -sl "$non_eq_dens_sl" -n ../../"$index" -b $((i*non_eq_thick_dt+1)) -e $(((i+1)*non_eq_thick_dt)) -o density_"$suffix".xvg > density_"$suffix".out 2>&1
                awk 'NR>24 {printf $1 "   " "%.4f\n",sum+=$2}' density_"$suffix".xvg > sum_"$suffix".txt
                echo '' >&4
            } &
        done
        cd ..
    fi
    # ↓ 提取输出non_eq过程的界面张力-时间曲线所需的原始数据
    if [ $non_eq_ift_dt ] && ((non_eq_ift_dt!=0));then
        mkdir IFT-t
        cd IFT-t
        let steps=time/non_eq_ift_dt
        for ((i=0;i<steps;i++))
        do
            {
                read -u4
                suffix="$((i*non_eq_ift_dt+1))"_"$(((i+1)*non_eq_ift_dt))"
                echo -e '#Surf*SurfTen'|gmx energy -f ../../non_eq.edr -b $((i*non_eq_ift_dt+1)) -e $(((i+1)*non_eq_ift_dt)) -o ift_"$suffix".xvg > ift_"$suffix".out 2>&1
                echo '' >&4
            } &
        done
        cd ..
    fi
    # ↓ 输出non_eq过程的界面厚度-时间曲线
    if [ $non_eq_thick_dt ] && ((non_eq_thick_dt!=0));then
        cd thickness-t
        let steps=time/non_eq_thick_dt
        echo -n '' > graph.txt
        wait
        for ((i=0;i<steps;i++))
        do
            echo -n "$((i*non_eq_thick_dt+1))"'     ' >> graph.txt
            suffix="$((i*non_eq_thick_dt+1))"_"$(((i+1)*non_eq_thick_dt))"
            let nr_middle=non_eq_dens_sl/2
            middle=`awk 'NR==nr {printf $2}' nr=$nr_middle sum_"$suffix".txt`
            end=`awk 'END {print $2}' sum_"$suffix".txt`
            level1=`echo | awk '{printf "%.10f",0.001*m}' m=$middle`
            level2=`echo | awk '{printf "%.10f",0.999*m}' m=$middle`
            level3=`echo | awk '{printf "%.10f",1.001*m}' m=$middle`
            level4=`echo | awk '{printf "%.10f",0.999*e}' e=$end`
            awk '{if ($2<y1) {x1=$1} if ($2<y2) {x2=$1} if ($2<y3) {x3=$1} if ($2<y4) {x4=$1}} END {printf "%.5f\n",(x2-x1+x4-x3)/2}' y1=$level1 y2=$level2 y3=$level3 y4=$level4 sum_"$suffix".txt >> graph.txt
        done
        cd ..
        echo 'Molecular Layer Thickness - Time Graph Generated @' $(date +%F%n%T) >> ../gmx_autorun_time.log
    fi
    # ↓ 输出non_eq过程的界面张力-时间曲线
    if [ $non_eq_ift_dt ] && ((non_eq_ift_dt!=0));then
        cd IFT-t
        let steps=time/non_eq_ift_dt
        echo -n '' > graph.txt
        wait
        for ((i=0;i<steps;i++))
        do
            suffix="$((i*non_eq_ift_dt+1))"_"$(((i+1)*non_eq_ift_dt))"
            echo -n "$((i*non_eq_ift_dt+1))"'     ' >> graph.txt
            cat ift_"$suffix".out | tail -1 | awk 'NR==1 {printf "%.5f",$2}' >> graph.txt
        done
        cd ..
        echo 'Interfacial Tension - Time Graph (rough) Generated @' $(date +%F%n%T) >> ../gmx_autorun_time.log
    fi
    density_func "$density" "$current_md" "$density_grp" "${density_sl:=200}" "$tbeg" "$index" &
    sasa_func "$sasa" "$sasa_grp" "$current_md" "$index" "${sasa_dt:=0}" "$last_mkdata" &
    rmsd_func "$rmsd" "$rmsd_grp" "$current_md" "$index" "${rmsd_dt:=0}" "$first_mkdata" "$last_mkdata" &
    cd ..
    # ↓ 非平衡-平衡MD模拟后的多窗口再平衡MD
    if (($non_eq_reeq==1));then
        mkdir non_eq_reeq
        cd non_eq_reeq
        let steps=time/non_eq_struc_dt
        cp -p ../non_eq.gro "$time".gro
        {
            read -u3
            echo -e 0 | gmx trjconv -f ../non_eq.xtc -s ../non_eq.tpr -pbc mol -dt $non_eq_struc_dt -sep -e $((time-1)) > trjconv.out 2>&1
            echo '' >&3
        } &
        if ((ana_parallel_wait==1));then
            wait
        fi
        echo -n '' > IFT_graph.txt
        echo -n '' > Total_energy_graph.txt
        for ((i=steps;i>0;i--))
        do
            let reeq_time=i*non_eq_struc_dt
            mkdir $reeq_time
            cd $reeq_time
            cp -p ../"$reeq_time".gro .
            if [ $interface_type == l_l ];then
                ensemble=npt
            else
                ensemble=nvt
            fi
            echo 'Re-equilibrium of Non-equilibrium MD Started @' $(date +%F%n%T) >> ../../gmx_autorun_time.log
            gmx grompp -f ../../eq_"$ensemble".mdp -c "$reeq_time".gro -p ../../mix.top -n ../../"$index" -o reeq.tpr
            gmx mdrun -v -deffnm reeq -pin on -nsteps $non_eq_reeq_nsteps -pinoffset $pinoffset -pinstride $pinstride $gpu $nt_set
            echo 'Re-equilibrium of Non-equilibrium MD Finished @' $(date +%F%n%T) >> ../../gmx_autorun_time.log
            echo -e '#Surf*SurfTen'|gmx energy -f reeq.edr -b $non_eq_reeq_data_begin -o ift.xvg > ift.out 2>&1
            echo -e 'Total-Energy\nTemperature\nPressure'|gmx energy -f reeq.edr -b $non_eq_reeq_data_begin -o thermo.xvg > thermo.out 2>&1
            IFT=`cat ift.out | tail -1 | awk 'NR==1 {printf "%.5f",$2}'`
            E_total=`cat thermo.out | tail -3 | awk 'NR==1 {print $3}'`
            cp -p reeq.gro ../reeq_"$reeq_time".gro
            cd ..
            echo "$reeq_time"'     '"$IFT" >> IFT_graph.txt
            echo "$reeq_time"'     '"$E_total" >> Total_energy_graph.txt
        done
        cd ..
    fi
    exec 4>&-
    echo 'Interfacial Tension - Time Graph (exact) Generated @' $(date +%F%n%T) >> gmx_autorun_time.log
    if [ ! "$last_mkdata" ];then
        first_mkdata="$current_md"
    fi
    last_mkdata="$current_md"
fi

# ↓ 产生相MD模拟
if (($prod==1));then
    if ((ana_parallel_wait==1));then
        wait
    fi
    current_md=prod
    echo 'Production MD Started @' $(date +%F%n%T) >> gmx_autorun_time.log
    gmx grompp -f prod.mdp -c "$structure" $cpt $ndx -p mix.top -o prod.tpr $mw
    unset mw
    gmx mdrun -v -deffnm prod -pin on -pinoffset $pinoffset -pinstride $pinstride $gpu $nt_set
    echo 'Production MD Finished @' $(date +%F%n%T) >> gmx_autorun_time.log
    structure='prod.gro'
    cpt='-t prod.cpt'
    if (($mkdata==1));then
        tmp=`cat prod.mdp | grep nsteps | head -1` && ns=${tmp#*'='}
        tmp=`cat prod.mdp | grep dt | head -1` && dt=${tmp#*'='}
        time=`echo | awk "{print $ns*$dt}"`
        if ((time<=length_prod));then
            let tbeg=0
        else
            let tbeg=time-length_prod
        fi
        mkdir prod_data
        cd prod_data
        sasa_func "$sasa" "$sasa_grp" "$current_md" "$index" "${sasa_dt:=0}" "$last_mkdata" &
        rmsd_func "$rmsd" "$rmsd_grp" "$current_md" "$index" "${rmsd_dt:=0}" "$first_mkdata" "$last_mkdata" &
        energy_func "$energy" "$current_md" "$tbeg" &
        density_func "$density" "$current_md" "$density_grp" "${density_sl:=200}" "$tbeg" "$index" &
        if (($msd==1));then
            for tmp in $msd_grp
            do
                {
                    read -u3
                    echo -e "$tmp\n$tmp"|gmx msd -f ../prod.xtc -s ../prod.tpr -n ../"$index" $msd_exoption -o msd_grp"$tmp".xvg > msd_grp"$tmp".out 2>&1
                    echo '' >&3
                } &
            done
        fi
        if (($rdf==1));then
            {
                read -u3
                gmx rdf -f ../prod.xtc -s ../prod.tpr -n ../"$index" -ref "$rdf_ref" -sel "$rdf_sel" -b $tbeg -rmax 2 > rdf.out 2>&1
                echo '' >&3
            } &
        fi
        fix_xtc_func "$fix_xtc" "$current_md" &
        hbond_func "$hbond" "$hbond_grp_a" "$hbond_grp_b" "$hbond_parallel_threads" "$current_md" "$index" "$hbond_exoption"
        if [ ! "$last_mkdata" ];then
            first_mkdata="$current_md"
        fi
        last_mkdata="$current_md"
        cd ..
        echo 'Production Data Analysis Finished @' $(date +%F%n%T) >> gmx_autorun_time.log
        if (($prod_rerun==1));then
            mkdir prod_rerun
            cd prod_rerun
            cp -p ../prod_rerun.mdp .
            if ((time<=rerun_length));then
                let tbeg_rerun=0
            else
                let tbeg_rerun=time-rerun_length
            fi
            echo 0|gmx trjconv -f ../prod.xtc -s ../prod.tpr -n ../"$index" -b "$tbeg_rerun" -o prod_last"$((time-tbeg_rerun))".xtc
            gmx grompp -f prod_rerun.mdp -c ../prod.gro -p ../mix.top -n ../"$index" -o prod_rerun.tpr
            gmx mdrun -v -deffnm prod_rerun -rerun prod_last"$((time-tbeg_rerun))".xtc -ntmpi "$rerun_ntmpi" -ntomp "$rerun_ntomp" -pin on -pinoffset $pinoffset -pinstride $pinstride
            echo 'Production rerun Finished @' $(date +%F%n%T) >> ../gmx_autorun_time.log
            echo -e "$rerun_energrp"|gmx energy -f prod_rerun.edr > energy.out 2>&1
            if (($rerun_rmxtc==1));then
                rm prod_last"$((time-tbeg_rerun))".xtc
            fi
            cd ..
        fi
    fi
fi

# ↓ 孤立体系MD模拟
if (($nopbc==1));then
    source $gmx_old_path/bin/GMXRC
    echo 'Nopbc MD Started @' $(date +%F%n%T) >> gmx_autorun_time.log
    gmx grompp -f nopbc.mdp -c nopbc.gro -p nopbc.top -o nopbc.tpr -maxwarn 1
    gmx mdrun -v -deffnm nopbc -c nopbc_out.gro -pin on -nt 1 -pinoffset $pinoffset
    echo 'Nopbc MD Finished @' $(date +%F%n%T) >> gmx_autorun_time.log
    mkdir nopbc_data
    cd nopbc_data
    echo -e 'Total-Energy'|gmx energy -f ../nopbc.edr -o nopbc_Total-Energy.xvg -b 1000 > nopbc_Total-Energy.out 2>&1
    cd ..
    echo 'Nopbc Data Analysis Finished @' $(date +%F%n%T) >> gmx_autorun_time.log
    source $gmx_cuda_path/bin/GMXRC
fi

# ↓ 关闭文件描述符
if ((mkdata==1));then
    exec 3>&-
fi

wait

echo 'GROMACS-Auto-Run Finished @' $(date +%F%n%T) >> gmx_autorun_time.log
echo 'GROMACS-Auto-Run Reminds You: Mission Accomplished !^_^!'  # 话虽这么说, 但明显可以看出来, 任务执行期间出错了也会告诉你“Mission Accomplished” [流汗黄豆]
