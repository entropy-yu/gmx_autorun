# GROMACS-Auto-Run

#### 介绍
Continuous operation of MD simulation and data processing tasks for surfactant systems under unattended conditions. About 1000 lines of Shell code.

#### 软件架构
一个主脚本（gmx_autorun.sh）和一个用于定义参数的子脚本（gmx_autorun.ini）。"_en"后缀指全英文注释版。


#### 安装教程

无需安装

#### 使用说明

下载源代码，根据喜好选择使用中文注释版或英文注释版，将2个脚本文件移入所运行任务的工作目录，重命名为gmx_autorun.*。根据gmx_autorun.ini中的说明，结合所运行任务的需求，正确地定义运行参数并运行。

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
